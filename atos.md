https://stackoverflow.com/questions/5842147/how-to-symbolicate-mac-osx-crash-reports-issued-by-apple/20701364#20701364

1) go to App Store Connect -> Specific app -> get dSYMs

2) unpack -> find dSYM for app (usually heaviest) -> Show package contents -> Conents/Resources/DWARF/<Your app name>
     If it is not your name (name of the lib) - you probably opened dSYM for a lib

3) copy it to some working dir

4) find info in crashed thread's stacktrace:

    ```
    .......

    Identifier:      de.metro.alex.customer.digitalcard.metro       <──┐
    Version:         3.16.2 (667)                                   <──┴── use these to download proper dSYM from App Store Connect
    Code Type:       ARM-64                     <- valuable bit!
    Parent Process:  ??? [1]

    .......

    Thread 0 Crashed:
    0   Companion                            0x00000001046aaf5c tableView + 308
    1   Companion                            0x00000001046aaeb4 tableView + 140
    2   Companion                            0x00000001046a9518 tableView + 132
            ^                                       ^               ^        ^
            |                                       |               |        |
       app (module) name                       A (address)    B (address)   C (offset)
                                                   (hex)          (hex)      (decimal)

    .......

    Binary Images:
    0x104210000 -        0x104a0ffff +Companion arm64  <path to binary>
    0x104d20000 -        0x104e77fff  Alamofire arm64  <path to binary>
    0x104ec0000 -        0x104f0bfff  AlamofireImage arm64  <path to binary>
        ^
        |
     B (address)
       (hex)

    .......
    ```
    If your crashlog is poorly symbolicated (like example above) - use `B` from 'Binary Images' section.

5) open terminal & navigate to your working dir (one with dSYM)

6) run this to get proper architecture identifier:

    ```
    $ lipo -info [YourApp]
    ```

7) run this to symbolicate line from stacktrace

    ```
    $ atos -o [YourApp] -arch [Code Type] -l [B] [A]
    ```

8) Example:

    ```
    $ lipo -info Companion

    Non-fat file: Companion is architecture: arm64

    $ atos -o Companion -arch arm64 -l 0x104210000 0x00000001046aaf5c

    specialized SearchArticlesRecentTermsViewController.tableView(_:didSelectRowAt:) (in Companion) (SearchArticlesRecentTermsViewController.swift:160)
    ```
